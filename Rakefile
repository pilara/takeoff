# frozen_string_literal: true

#
# Rakefile for Chef Server Repository
#
# Author:: Adam Jacob (<adam@opscode.com>)
# Copyright:: Copyright (c) 2008 Opscode, Inc.
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
Rake::TaskManager.record_task_metadata = true

require 'rubygems'
require 'json'
require 'io/wait'
require 'fileutils'

%w[./lib/*.rb ./lib/steps/*.rb ./config/*.rb].each do |dir|
  Dir[dir].each { |file| require file }
end

include CommandRunner

def start_chef(roles)
  run_command_on_roles roles,
                       'sudo service chef-client start',
                       title: "Starting chef-client on #{roles.join(', ')}",
                       silence_stdout: false
end

def parse_pending_migrations_output(output)
  found = []

  output.each_line do |line|
    status, migration = line.strip.split(/\s+/, 2)

    found << migration if status == 'down'
  end

  unless found.empty?
    puts 'The following migrations are pending:'
    puts
    puts found
  end
end

def yield_and_wait_until_reload(role, role_services)
  since = Time.now
  services_to_check = role_services.map { |service| "sudo gitlab-ctl status #{service}" }

  yield

  loop do
    sleep(5) unless Takeoff.config[:dry_run]
    cmd = "bundle exec knife ssh -a ipaddress 'roles:#{role}' " \
          "\"#{services_to_check.join(' && ')}\""

    cmd = "echo #{cmd}" if Takeoff.config[:dry_run]

    out = `#{cmd}`
    now = Time.now

    # Bail out once everything has restarted since the given time.
    all_restarted = out.scan(/(\d+)s; run:/).all? do |match|
      (now - match[0].to_i) >= since
    end

    break if all_restarted
  end
end

namespace :service do
  desc 'Get the service status'
  task :status, %i[service environment] do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles'][args.service]
    run_command_on_roles roles,
                         "sudo gitlab-ctl status #{args.service}",
                         title: "Getting #{args.service} status on #{roles.join(', ')}",
                         silence_stdout: false
  end

  desc 'Restart the service'
  task :restart, %i[service environment] do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles'][args.service]
    run_command_on_roles roles,
                         "sudo gitlab-ctl restart #{args.service}",
                         title: "Restarting #{args.service} status on #{roles.join(', ')}"
  end

  desc 'Start the service'
  task :start, %i[service environment] do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles'][args.service]
    run_command_on_roles roles,
                         "sudo gitlab-ctl start #{args.service}",
                         title: "Starting #{args.service} status on #{roles.join(', ')}"
  end

  desc 'Stop the service'
  task :stop, %i[service environment] do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles'][args.service]
    if roles
      run_command_on_roles roles,
                           "sudo gitlab-ctl stop #{args.service}",
                           title: "Stopping #{args.service} status on #{roles.join(', ')}"
    end
  end
end

namespace :deploy_page do
  desc 'Check the status of the deployment page'
  task :status, :environment do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles']['deploy-page']

    run_command_on_roles roles,
                         'sudo gitlab-ctl deploy-page status',
                         title: "Checking the deploy page status on #{roles.join(', ')}",
                         silence_stdout: false
  end

  desc 'Enable the deployment page and takes GitLab.com offline'
  task :enable, :environment do |_, args|
    args.with_defaults(environment: 'production')

    print 'This will result in downtime, are you sure you want this? [y/n]: '
    next if STDIN.gets.strip.casecmp('y') != 0

    puts 'Enabling deploy page'
    roles = Takeoff.env[args.environment]['service_roles']['deploy-page']
    run_command_on_roles roles,
                         'sudo gitlab-ctl deploy-page up',
                         title: "Enabling the deploy page on #{roles.join(', ')}"

    # puts 'Stopping unicorn'
    # run_job_on_role(args.role, 'unicorn-stop')
  end

  desc 'Disable the deployment page and put GitLab.com back online'
  task :disable, :environment do |_, args|
    args.with_defaults(environment: 'production')
    roles = Takeoff.env[args.environment]['service_roles']['deploy-page']

    run_command_on_roles roles,
                         'sudo gitlab-ctl start',
                         title: "Starting GitLab services on #{roles.join(', ')}"

    run_command_on_roles roles,
                         'sudo gitlab-ctl deploy-page down',
                         title: "Disabling the deploy page on #{roles.join(', ')}"
  end
end

desc 'Deploy version to environment'
task :deploy, :environment, :version, :repo do |_t, args|
  args.with_defaults(repo: 'gitlab/pre-release')
  puts "Deploying #{args.repo} v#{args.version} to #{args.environment}"

  StepRunner.new(args.version, args.environment, args.repo).run
end

def dpkg_gitlab_version(roles)
  stdout, = run_command_on_roles roles,
                                 'dpkg-query --show gitlab-ee',
                                 title: "Checking the GitLab version on #{roles}",
                                 silence_stdout: false
  stdout
end

def hosts_per_version(roles)
  stdout = dpkg_gitlab_version(roles)

  ## This logic should be reinstated after adapting it to the new output format
  hosts_per_version = Hash.new { |hash, key| hash[key] = [] }
  stdout.each_line do |line|
    host, version = line.strip.split.values_at(0, 2)
    hosts_per_version[version] << host
  end

  hosts_per_version
end

namespace :check do
  desc 'Checks the GitLab version for a given role'
  task :version, :environment, :role do |_, args|
    roles = Roles.new(args.environment)
    args.with_defaults(role: roles.regular_roles)

    hosts_per_version = hosts_per_version(args.role)

    if hosts_per_version.length > 1
      puts ANSI::RED % 'Hosts running different versions:'
      hosts_per_version.each do |version, hosts|
        print ANSI::BOLD % "#{version}: "
        puts hosts.join(' ')
      end
    elsif hosts_per_version.length == 1
      puts "New version: #{hosts_per_version.keys.first}"
    else
      abort ANSI::RED % 'No versions found on any of the hosts'
    end
  end

  desc 'Checks the process run times for a given role'
  task :processes, :environment, :role do |_, args|
    args.with_defaults(environment: 'production')

    roles = Roles.new(args.environment)
    args.with_defaults(role: roles.base_role)

    services_to_check = roles.services_for_role(args.role)

    abort if services_to_check.empty?

    check_cmd = "sudo gitlab-ctl status | grep \"^#{services_to_check.join('\|')}\""

    puts run_command_on_roles args.role,
                              check_cmd,
                              title: "Checking the GitLab services on #{args.role}",
                              silence_stdout: false
  end

  desc 'Checks for pending migrations for production'
  task :migrations, :environment do |_task, args|
    args.with_defaults(environment: 'production')
    blessed_node = Takeoff.env[args.environment]['blessed_node']
    stdout, = run_command_on_roles blessed_node,
                                   'sudo gitlab-rake db:migrate:status',
                                   title: "Checking for pending migrations on #{args.environment}"
    parse_pending_migrations_output(stdout)
  end
end

desc 'Follow db-migrate log files with tail'
task :follow_migrations, :environment do |_t, args|
  blessed_node = Takeoff.env[args.environment]['blessed_node']
  command = 'sudo find /var/log/gitlab/gitlab-rails -type f -name "*db-migrate*log" | sort | tail -1 | sudo xargs tail -f'
  knife_command = "bundle exec knife ssh -e -a ipaddress 'roles:#{blessed_node}' '#{command}'"

  puts 'Following database migrations:'
  puts "> #{knife_command}" if Takeoff.config[:verbose]

  system(knife_command)
end

### Other stuff to review

def nodes_on_role(role)
  `knife search node 'roles:#{role}' -i 2>/dev/null`.split
end

desc 'Show uptime of all gitlab.com workers'
task :worker_uptime do
  run_role 'gitlab-cluster-worker',
           'uptime',
           title: 'Gathering worker uptimes',
           silence_stdout: false
end
