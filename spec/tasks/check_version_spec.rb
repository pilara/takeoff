# frozen_string_literal: true

require 'spec_helper'

describe 'rake check:version', type: :task do
  before { enable_dry_run }

  it 'outputs a single version for the same hosts' do
    allow_open3_to_return(true, ['1.1.1.1 gitlab-ee 10.0.2', '1.1.1.2 gitlab-ee 10.0.2'])

    expect_task_to_complete

    expect { task.invoke('staging') }.to output(/New version: 10.0.2/).to_stdout_from_any_process
  end
end
