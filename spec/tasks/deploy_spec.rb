# frozen_string_literal: true

require 'spec_helper'

describe 'rake deploy', type: :task do
  let(:version_output) { "10.196.8.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.2.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.4.101 gitlab-ee\t10.0.2-ee.0\r\n" }

  before { allow_open3_to_return(true) }
  before { enable_dry_run }

  before do
    allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
  end

  it 'outputs a single version for the same hosts' do
    allow_any_instance_of(Steps::LoadBalancingCheck).to receive(:blessed_node_role_path).and_return('spec/tasks/roles/staging-base-deploy-node.json')

    expect_task_to_complete

    expect { task.invoke('staging', '10.0.2-ee.0') }.to output(/New version: 10.0.2-ee.0/).to_stdout_from_any_process
  end

  it 'runs without errors on canary' do
    expect_task_to_complete

    task.invoke('canary', '10.0.2')
  end

  it 'runs without errors on production' do
    expect_task_to_complete

    task.invoke('production', '10.0.2')
  end
end
