# frozen_string_literal: true

require 'spec_helper'
require './lib/step_runner'

describe StepRunner do
  let(:version_output) { "10.196.8.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.2.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.4.101 gitlab-ee\t10.0.2-ee.0\r\n" }
  let(:runner_output) do
    <<~OUTPUT
      git checkout master
      git pull git@gitlab.com:gitlab-org/takeoff.git master
      bundle exec knife download roles/staging-base-deploy-node.json
      Ignoring load balancing check
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-be-sidekiq OR roles:staging-base-be-mailroom OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo service chef-client stop
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl stop mailroom
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl stop sidekiq-cluster
      bundle exec knife download roles/staging-omnibus-version.json
      Ignoring version update to staging
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart registry
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart nginx
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake gitlab:track_deployment
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-be-sidekiq OR roles:staging-base-be-mailroom OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo -E service chef-client start
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl start
      bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl deploy-page down
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate:status
      New version: 10.0.2-ee.0
    OUTPUT
  end

  before do
    enable_dry_run

    allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
  end

  subject { described_class.new('10.0.2-ee.0', 'staging', 'gitlab/test') }

  it 'outputs the right command' do
    expect(subject).not_to receive(:abort)

    expect { subject.run }.to output(runner_output).to_stdout_from_any_process
  end

  context 'error starting chef' do
    let(:retry_runner_output) do
      <<~OUTPUT
        git checkout master
        git pull git@gitlab.com:gitlab-org/takeoff.git master
        bundle exec knife download roles/staging-base-deploy-node.json
        Ignoring load balancing check
        Retrying Steps::ChefStop - Attempt 1 - Error: test error
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl stop mailroom
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl stop sidekiq-cluster
        bundle exec knife download roles/staging-omnibus-version.json
        Ignoring version update to staging
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=1 gitlab-rake db:migrate
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake gitlab:track_deployment
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR roles:staging-base-be-sidekiq OR roles:staging-base-be-mailroom OR roles:staging-base-fe-web OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-registry OR roles:staging-base-deploy-node sudo -E service chef-client start
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl start
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl deploy-page down
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo gitlab-rake db:migrate:status
        New version: 10.0.2-ee.0
      OUTPUT
    end

    it 'shows the error and retry attempt' do
      allow_any_instance_of(Steps::ChefStop).to receive(:roles_to_stop).and_raise('test error')

      expect_any_instance_of(Steps::ChefStop).to receive(:abort).once

      expect { subject.run }.to output(retry_runner_output).to_stdout_from_any_process
    end
  end
end
