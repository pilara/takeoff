# frozen_string_literal: true

require 'spec_helper'
require './lib/post_checks/retry_on_failure'
require './lib/steps/chef_start'

describe PostChecks::RetryOnFailure do
  before { enable_dry_run }
  let(:step) { Steps::ChefStart.new(Roles.new('staging')) }

  it 'retries on error' do
    allow(step).to receive(:error).and_return('error')

    subject = described_class.new('test error', step: step)

    command = <<~COMMAND
      Retrying Steps::ChefStart - Attempt 1 - Error: error
      bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo -E chef-client
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end

  it 'does not abort when there is no error' do
    subject = described_class.new(nil, step: step)

    expect_any_instance_of(PostChecks::RetryOnFailure).not_to receive(:abort)
    allow(step).to receive(:error).and_return('test error 2')

    expect { subject.run }.to output('').to_stdout_from_any_process
  end
end
