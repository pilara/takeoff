# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/post_deploy_mediator'

describe Steps::PostDeployMediator do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq OR roles:staging-base-fe-api OR roles:staging-base-fe-git OR roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
      COMMAND
    end
  end
end
