# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/sidekiq_restart'

describe Steps::SidekiqRestart do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end
