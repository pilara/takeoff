# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/deploy_mediator'

describe Steps::DeployMediator do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging'), version: '10.0.2-ee.0') }

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end

  context 'with custom node' do
    it 'outputs the right command' do
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'staging-base-fe-web')

      expect { subject.run && $stdout.flush }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-git sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end
end
