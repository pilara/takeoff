# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/sidekiq_hup'

describe Steps::SidekiqHup do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end
end
