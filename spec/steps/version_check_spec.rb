# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/version_check'

describe Steps::VersionCheck do
  let(:version_output) { "10.196.8.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.2.101 gitlab-ee\t10.0.2-ee.0\r\n10.196.4.101 gitlab-ee\t10.0.2-ee.0\r\n" }
  subject { described_class.new(Roles.new('staging')) }

  before do
    enable_dry_run
    allow_any_instance_of(Steps::VersionCheck).to receive(:dpkg_gitlab_version).and_return(version_output)
  end

  it 'outputs the right version' do
    expect { subject.run! }.to output("New version: 10.0.2-ee.0\n").to_stdout_from_any_process
  end
end
