# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/load_balancing_check'

describe Steps::LoadBalancingCheck do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  it 'ignores check on dry run' do
    expect { subject.run }.to output(/Ignoring load balancing check/).to_stdout_from_any_process
  end

  it 'downloads the JSON' do
    expect { subject.run }.to output(%r{bundle exec knife download roles/staging-base-deploy-node.json}).to_stdout_from_any_process
  end
end
