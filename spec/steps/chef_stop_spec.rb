# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/chef_stop'

describe Steps::ChefStop do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  it 'outputs the right command' do
    command = <<~COMMAND.strip.tr("\n", ' ')
      bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR
      roles:staging-base-be-sidekiq OR roles:staging-base-be-mailroom OR
      roles:staging-base-fe-web OR roles:staging-base-fe-api OR
      roles:staging-base-fe-git OR roles:staging-base-fe-registry OR
      roles:staging-base-deploy-node sudo service chef-client stop
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end
end
