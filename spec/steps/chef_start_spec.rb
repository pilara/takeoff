# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/chef_start'

describe Steps::ChefStart do
  before { enable_dry_run }

  context 'blessed node' do
    subject { described_class.new(Roles.new('staging')) }

    it 'outputs the right command' do
      command = <<~COMMAND.strip.tr("\n", ' ')
        bundle exec knife ssh -e -a ipaddress roles:staging-base-deploy-node sudo -E chef-client
      COMMAND

      expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
    end
  end

  context 'other nodes' do
    let(:roles) { Roles.new('staging') }
    subject { described_class.new(roles, roles_to_start: roles.regular_roles) }

    it 'outputs the right command' do
      command = <<~COMMAND.strip.tr("\n", ' ')
        bundle exec knife ssh -e -a ipaddress roles:staging-base-stor-nfs OR
        roles:staging-base-be-sidekiq OR roles:staging-base-be-mailroom OR
        roles:staging-base-fe-web OR roles:staging-base-fe-api OR
        roles:staging-base-fe-git OR roles:staging-base-fe-registry
        sudo -E service chef-client start
      COMMAND

      expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
    end
  end
end
