# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/sidekiq_stop'

describe Steps::SidekiqStop do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('staging')) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-mailroom sudo gitlab-ctl stop mailroom
      bundle exec knife ssh -e -a ipaddress roles:staging-base-be-sidekiq sudo gitlab-ctl stop sidekiq-cluster
    COMMAND

    expect { subject.run }.to output(command).to_stdout_from_any_process
  end
end
