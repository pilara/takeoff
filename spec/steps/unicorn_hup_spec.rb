# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/unicorn_hup'

describe Steps::UnicornHup do
  before { enable_dry_run }

  context 'base role' do
    subject { described_class.new(Roles.new('staging'), roles_to_hup: 'staging-base-fe-web') }

    it 'outputs the right command for the base fe web role' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web sudo gitlab-ctl hup unicorn
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'array of roles' do
    subject { described_class.new(Roles.new('staging'), roles_to_hup: ['staging-base-fe-web', 'gitlab2-base-fe-web']) }

    it 'outputs the right command for an array of roles' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a ipaddress roles:staging-base-fe-web OR roles:gitlab2-base-fe-web sudo gitlab-ctl hup unicorn
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end
end
