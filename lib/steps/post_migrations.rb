# frozen_string_literal: true

require './lib/steps/migrations'

module Steps
  class PostMigrations < Migrations
    protected

    def migrate_command
      'sudo gitlab-rake db:migrate'
    end

    def title
      'Running post-deployment migrations from the blessed node'
    end
  end
end
