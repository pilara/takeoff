# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class ServicesRestart < BaseRoles
    def run
      if roles.services_for_role(role).include?('unicorn')
        custom_services.each { |service| restart_services(service) }
      else
        restart_services
      end
    end

    private

    def role
      @options[:role]
    end

    def custom_services
      roles.services_for_role(role) - ['unicorn']
    end

    def restart_services(service = nil)
      cmd = 'sudo gitlab-ctl restart'
      cmd += " #{service}" if service

      run_command_on_roles role,
                           cmd,
                           title: "Restarting #{service_text(service)} on #{role}"
    end

    def service_text(service)
      service ? service : 'services'
    end
  end
end
