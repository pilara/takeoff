# frozen_string_literal: true

require './lib/post_checks'

module Steps
  class Base
    attr_reader :options
    private :options

    attr_reader :error

    def initialize(options = {})
      @options = options
      @error = nil
    end

    def run!
      pre_checks

      begin
        run
      rescue => e
        @error = e.message
      end

      post_checks
    end

    def restart
      @error = nil

      begin
        run
      rescue => e
        @error = e.message
      end
    end

    def run_with_progress(*args)
      CommandRunner.run_with_progress(*args)
    end

    def pre_checks; end

    def post_checks
      return unless options['post_checks']

      options['post_checks'].each do |post_check|
        PostChecks[post_check].new(@error, step: self).run
      end

      abort(@error) if @error
    end

    def abort(message)
      # TODO: use own error class, rename this method
      raise ScriptError, message
    end
  end
end
