# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class VersionCheck < BaseRoles
    def run
      if hosts_per_version.length > 1
        different_version
      elsif hosts_per_version.length == 1
        puts "New version: #{hosts_per_version.keys.first}"
      else
        puts ANSI::RED % 'No versions found on any of the hosts'
        abort unless Takeoff.config[:dry_run]
      end
    end

    private

    def different_version
      puts ANSI::RED % 'Hosts running different versions:'

      hosts_per_version.each do |version, hosts|
        print ANSI::BOLD % "#{version}: "
        puts hosts.join(' ')
      end
    end

    def hosts_per_version
      @hosts_per_version ||=
        begin
          stdout = dpkg_gitlab_version(role)

          ## This logic should be reinstated after adapting it to the new output format
          hosts_per_version = Hash.new { |hash, key| hash[key] = [] }

          stdout.each_line do |line|
            next unless line =~ /\A[0-9].*/

            host, version = line.strip.split.values_at(0, 2)
            hosts_per_version[version] << host
          end

          hosts_per_version
        end
    end

    def dpkg_gitlab_version(roles)
      stdout, = run_command_on_roles roles,
                                     'dpkg-query --show gitlab-ee',
                                     title: "Checking the GitLab version on #{roles}",
                                     silence_stdout: false
      stdout
    end

    def role
      @options[:role] || roles.regular_roles
    end
  end
end
