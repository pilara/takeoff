# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class DeployPackage < BaseRoles
    def run
      run_command_on_roles roles_to_deploy,
      "sudo apt-get -qq update && sudo apt-get install -y -q --force-yes gitlab-ee=#{version}",
      title: title
    end

    private

    def roles_to_deploy
      options[:roles_to_deploy] ||= roles.blessed_node
    end

    def title
      options[:roles_to_deploy] ? "Deploying gitlab-ee #{version} on #{roles_to_deploy}" : "Deploying gitlab-ee #{version} on the blessed node"
    end

    def version
      @options[:version]
    end
  end
end
