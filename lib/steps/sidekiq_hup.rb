# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  # Disable non-crucial services as soon as possible. This ensures they don't
  # slow down any migrations.
  class SidekiqHup < BaseRoles
    def run
      # Skip on canary as there are no sidekiq roles
      return if sidekiq_roles.empty?

      run_command_on_roles sidekiq_roles,
                           'sudo /opt/gitlab/init/sidekiq 1 || sudo /opt/gitlab/init/sidekiq-cluster 1',
                           title: 'Sending HUP to sidekiq and sidekiq-cluster'

      sleep 10 unless Takeoff.config[:dry_run]
    end

    private

    def sidekiq_roles
      @sidekiq_roles ||= roles.sidekiq_roles
    end
  end
end
