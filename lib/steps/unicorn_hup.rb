# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class UnicornHup < BaseRoles
    def run
      return unless suitable_roles_to_hup?

      run_command_on_roles roles_to_hup,
                           'sudo gitlab-ctl hup unicorn',
                           title: "Sending HUP to unicorn on #{roles_to_hup}"
    end

    def suitable_roles_to_hup?
      roles.service_roles['unicorn'].include?(roles_to_hup) || roles_to_hup.is_a?(Array)
    end

    private

    def roles_to_hup
      options[:roles_to_hup]
    end
  end
end
