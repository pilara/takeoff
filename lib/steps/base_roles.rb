# frozen_string_literal: true

require './lib/steps/base'

module Steps
  class BaseRoles < Base
    attr_reader :roles
    private :roles

    def initialize(roles, options = {})
      @roles = roles

      super(options)
    end

    private

    def run_command_on_roles(*args)
      CommandRunner.run_command_on_roles(*args)
    end
  end
end
