# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/steps/deploy_package'
require './lib/steps/unicorn_hup'
require './lib/steps/services_restart'

module Steps
  class DeployMediator < BaseRoles
    # TODO: Refactor this class

    def run
      roles.regular_roles.each do |role|
        yield_and_wait_until_reload(role, roles.services_for_role(role)) do
          steps = [Steps::DeployPackage.new(roles, roles_to_deploy: role, version: version),
          Steps::UnicornHup.new(roles, roles_to_hup: role),
          Steps::ServicesRestart.new(roles, role: role)]

          steps.each(&:run!)

          errors = steps.map(&:error).compact

          @error = errors.join(', ') if errors.any?
        end
      end
    end

    private

    def yield_and_wait_until_reload(role, role_services)
      since = Time.now
      services_to_check = role_services.map { |service| "sudo gitlab-ctl status #{service}" }

      yield

      loop do
        sleep(5) unless Takeoff.config[:dry_run]
        cmd = "bundle exec knife ssh -a ipaddress 'roles:#{role}' " \
          "\"#{services_to_check.join(' && ')}\""

        cmd = "echo #{cmd}" if Takeoff.config[:dry_run]

        out = `#{cmd}`
        now = Time.now

        # Bail out once everything has restarted since the given time.
        all_restarted = out.scan(/(\d+)s; run:/).all? do |match|
          (now - match[0].to_i) >= since
        end

        break if all_restarted
      end
    end

    def version
      @options[:version]
    end
  end
end
