# frozen_string_literal: true

require './config/takeoff'

class Roles
  def initialize(environment)
    @roles = Takeoff.env[environment]
  end

  def regular_roles
    service_roles.values.flatten.uniq
  end

  def sidekiq_roles
    [service_roles['sidekiq'], service_roles['sidekiq-cluster']].flatten.compact
  end

  def regular_and_blessed
    regular_roles + [blessed_node]
  end

  def services_for_role(role)
    service_roles.each_with_object([]) do |(k, v), services|
      services << k if v.include? role
    end
  end

  private

  def method_missing(name, *args, &block)
    @roles.key?(name.to_s) ? @roles[name.to_s] : super
  end

  def respond_to_missing?(name, _include_private = false)
    @roles.key?(name)
  end
end
